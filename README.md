# Connectors Backend Community

Welcome to the UTU bounty programme for creating social media connectors.  This project stores 
backend contributions for creating a social media connector. Another repo called 
connectors-frontend-community holds the frontend code.

## Instructions

(i) Get in touch with us our discord server and find out which connectors are remaining that you
    can code and what the bounty is for coding a connctor.

  The UTU discord server invite link is:

        https://discord.gg/dFQ588vB

    You can contact:

        JD (SYT) / jd_syt  

(ii)  cd ~

(iii) mkdir workspace

(iv) cd workspace

(v) git clone https://gitlab.com/ututrust/api-sdk/connectors-backend-community.git

(vi) cd connectors-backend-community

(vii) Create your branch.  The first bit of your branch name should be the name of the social media 
platform you are creating the connector for, followed by an underscore and your initials. For 
example if you are creating a connector for linkedin and your initials are jb, create the branch 
as follows:

      e.g. git checkout -b linkedin_jb

(viii)  Create a react nestjs project using TypeScript:

      npx create-react-app . --template typescript 

(ix) Follow instructions here to create your rewired configuration:

  https://www.npmjs.com/package/react-app-rewired

(x) Code the backend side of your app in React and TypeScript

(xi)  Add an INSTRUCTIONS.md file with instructions of how to run your backend code.  Provide 
instructions of any environmental properties you need to add to a .env file

## API

Your frontend code will call a nestjs backend to retrieve the connections.

You will expose the following API on your backend code.  Your Frontend code whill make a call to:

    getConnections( connectionRequest: any ): ConnectionsResponse

connectionRequest passes any info required by the connector.  For example in the case of the linkedin
connector, there is a button which once clicked opens a linkedin login in a window.  On completion
of the login a redirect url is called and an access code appended on the end of the url by Linkedin.  

In the linkedin frontend example the accessCode is taken from the url and sent in the 
connectionRequest to the backend.

connectionsResponse.ts looks like:

    import { Connection } from "./connection";

    export class ConnectionsResponse {

      constructor(
        public connections: Connection[]) { }
    }

connection.ts looks like:

    export class Connection {

    constructor(
      public firstName: string,
      public lastName: string,
      public socialMediaHandle: string
    ) { }
}

Note that in the case of linkedin the socialMediaHandle is an email address.  In the case of 
Telegram it is a telephone number.

## Note

(i) There is already a react rewired branch for linkedin in this repo:

    linkedin_jd_bb

There is a similarly named branch on the frontend repo:

    connectors-frontend-community

(ii) Do not merge your branch to the main branch.  The main branch should only have this README.md
The idea is that each example of a connector lives on its own branch.


  
